<?php
include_once('config.php');

if( !isset( $_GET['lang'])) { include_once('lang/registerNL.php'); } else {
	switch( $_GET['lang'] ) {
		case "en" : include_once('lang/registerEN.php'); break;
		case "nl" : 
		default : include_once('lang/registerNL.php'); break;
	}
}

IF( isset($_POST['controle']) AND $_POST['controle']=="TRUE") {

$con = mysql_connect($SQLhost, $SQLuser, $SQLpass) or die('Could not connect: ' . mysql_error()); 
mysql_select_db($SQLdb, $con) or die('Could not find database: ' . mysql_error()) ; 

$taken = mysql_query("SELECT id FROM user WHERE Username = '".$_POST['username']."' ");

$leeftijd = calcAge($_POST['dag'], $_POST['maand'], $_POST['jaar']);

if (
   (empty($_POST['username']))
OR (empty($_POST['password1']))
OR (empty($_POST['password2']))
OR (($_POST['password1']) != ($_POST['password2']))
OR (empty($_POST['emailadres']))
OR (($_POST['dag'] == "1") AND ($_POST['maand'] == "1") AND ($_POST['jaar'] == date("Y") ) )
OR ( $leeftijd < "18")
OR ( mysql_num_rows($taken) > 0)
)
	{ $error_head=("<b>!!  ERROR  !!</b>");
	$error="1";}

IF( isset($error) AND $error==1) {
	IF (empty($_POST['username']))
		{ $error_user = $error_user_empty; }
	IF (mysql_num_rows($taken) > 0) 
		{ $error_user = $error_user_taken; }
	IF (($_POST['password1']) != ($_POST['password2']))
		{ $error_pass1 = $error_pass_diff; }
	IF ((empty($_POST['password1'])) OR (empty($_POST['password2'])))
		{ $error_pass2 = $error_pass_empty; }
	IF ((empty($_POST['password1'])) AND (empty($_POST['password2'])))
		{ $error_pass1 = $error_pass2 = $error_pass_empty_both; }
	IF (empty($_POST['emailadres']))
		{ $error_email = $error_email_empty; }
	IF (($_POST['dag'] == "1") AND ($_POST['maand'] == "1") AND ($_POST['jaar'] == date("Y")))
		{ $error_leeftijd1= $error_leeftijd_empty; }
	IF (($leeftijd < 18 ) AND ($leeftijd >= 0) AND (!isset($error_leeftijd1)))
		{ $error_leeftijd2 = $error_leeftijd_tejong; }		
}

else {
session_start();
$_POST['md5_password']=md5($_POST['password2']);
$_SESSION['username']=$_POST['username'];
$_SESSION['emailadres']=$_POST['emailadres'];
$_SESSION['dag']=$_POST['dag'];
$_SESSION['maand']=$_POST['maand'];
$_SESSION['jaar']=$_POST['jaar'];
$salt = "salt";
$defbg = "00BFFF";

$sql_use = "INSERT INTO user (Username, Password, salt, Email, Registered, Birthday, background, Profielbericht, picture)
			VALUES('".mysql_real_escape_string($_POST['username'])."', '".$_POST['md5_password'] . $salt . "', 
				'".$salt."', '".mysql_real_escape_string($_POST['emailadres'])."', NOW(), 
				'". $_POST['jaar'] . "-" . $_POST['maand'] . "-" . $_POST['dag'] . "', '".$defbg."', '', '0.png' )";
				
if (!mysql_query($sql_use))
	{ die('Error: ' . mysql_error($con));}

if (!( mkdir('profiles/' . $_POST['username'] . '/') ) )
	{ die ('Error making directory.'); }
	
header("location: registered.php");

mysql_close($con); }
}
?>

<html>
<head>
<title><?php echo $lang["registration"]; ?> - <?php echo $sitename; ?> </title>
<link rel="icon" type="image/ico" href="/dating/favicon.ico"> </link>
</head>
<body bgcolor="00BFFF">
<center><a href="<?php echo $homepage; ?>"><img border="0" src="profiles/logo.png" alt="<?php echo $sitename; ?> logo" width="435" height="264"></a></center> <br />
<form action="" method="post">
<center> <big><big><?php echo $lang["register"]; ?></big></big> </center><br /><hr>
<?php if(isset($error_head)){echo $error_head;} ?><br /></font>
<table> 
<!-- Normale Gegevens -->
<tr><td width="200"><?php echo $lang["username"]; ?></td>
	<td width="100"><input type="text" name="username" value="<?php if(isset($_POST['username'])){echo $_POST['username'];}?>" size="27"></td>
	<td width="10"></td> <td width="352"> <?php if( isset($error_user) ){echo $error_user;} ?> </td> </tr>
<tr><td><?php echo $lang["password"]; ?></td><td><input type="password" name="password1" value="<?php if(isset($_POST['password1'])){echo $_POST['password1']; } ?>" size="27" /> </td>
	<td></td> <td> <?php if( isset($error_pass1)){echo $error_pass1;} ?> </td> </tr>
<tr><td><?php echo $lang["controle"]; ?></td><td><input type="password" name="password2" value="<?php if(isset($_POST['password2'])){echo $_POST['password2']; } ?>" size="27" /> </td>
	<td></td> <td> <?php if( isset($error_pass2)){echo $error_pass2;} ?> </td></tr>
<tr><td><?php echo $lang["email"]; ?> </td><td> <input type="email" name="emailadres" value="<?php if(isset($_POST['emailadres'])){echo $_POST['emailadres']; } ?>" size="27" / ><br /></td>
	<td></td> <td> <?php if( isset($error_email)){echo $error_email;} ?> </td></tr>
</table>

<table> <!-- Geboortedatum -->
	<tr><td width="200"><?php echo $lang["geboorte"]; ?></td><td width="198"> 
<?php

//selectbox van de dagen
echo '<select name="dag">';
for($d=1;$d<=31;$d++){
   if($d == $_POST["dag"]){$selected = 'selected="selected"';}else{$selected = '';}
   echo '<option value="'.$d.'" '.$selected.'>'.$d.'</option>'; }
echo '</select>';

//selectbox van de maanden
//gebruik $maanden[$m-1] omdat een array bij 0 begint
echo '<select name="maand">';
for($m=1;$m<=12;$m++){
   if($m == $_POST["maand"]){$selected = 'selected="selected"';}else{$selected = '';}
   echo '<option value="'.$m.'" '.$selected.'>'.$maanden[$m-1].'</option>'; }
echo '</select>';

//selectbox van de jaren
echo '<select name="jaar">';
for($j=date("Y");$j>=1940;$j--){
   if($j == $_POST["jaar"]){$selected = 'selected="selected"';}else{$selected = '';}
   echo '<option value="'.$j.'" '.$selected.'>'.$j.'</option>'; }
echo '</select>';
?>

<td width="10"></td> <td width="352"> 

<?php 
if(isset($error_leeftijd1)){ echo $error_leeftijd1; } 
if(isset($error_leeftijd2)){ echo $error_leeftijd2; } 
?> </td> </tr>

</table><table>
<tr><td width="200"></td><td width="350"> <?php echo $lang["leeftijd"]; ?> </td></tr>
</table>
<input type="hidden" name="controle" value="TRUE">
<input type="submit" value=<?php echo $lang["submit"]; ?> ></form>
<hr>
<form method="post" action="" >
<input type="submit" name="submit" value= <?php echo $lang["reset"]; ?> ></form>
</body>
</html>