<?php
session_start();
include_once('config.php');

if( !isset( $_GET['lang'])) { 
	$_GET['lang'] = "nl";	
} 
switch( $_GET['lang'] ) {
	case "en" : 
		include_once('lang/resultEN.php'); 
		include_once('lang/arrayEN.php');
		break;
	case "nl" : 
	default : 
		include_once('lang/resultNL.php'); 
		include_once('lang/arrayNL.php');
		break;
}

IF (!isset($_SESSION['username'])) { header('Location: '. $homepage); }
IF ($_SESSION['query']== "") { header('Location: matching.php'); }
	
$con = mysql_connect($SQLhost, $SQLuser, $SQLpass) or die('Could not connect: ' . mysql_error()); 
mysql_select_db($SQLdb, $con) or die('Could not find database: ' . mysql_error()) ; 

IF (isset($_POST['controle']) AND $_POST['controle'] == "TRUE") {
$sql_order = "ORDER BY ";
if ($_POST['sort'] == "Jaar") {
	if ($_POST['way'] == "ASC") { $sql_order .= "Jaar DESC, Maand DESC, Dag DESC "; }
	else { $sql_order .= "Jaar ASC, Maand ASC, Dag ASC"; }
} else {
$sql_order .= $_POST['sort'] ." " . $_POST['way'] . " ";
}

IF (! ($_SESSION['order'] == $sql_order ))
	{ $_SESSION['order'] = $sql_order; }
} ELSE {
$_SESSION['order'] = "ORDER BY Birthday DESC";
}

$res = mysql_query( $_SESSION['query'] . " " . $_SESSION['order'] );

?>

<html>
<head>
<title><?php echo $lang['match']; ?> - <?php echo $_SESSION['username']; ?> </title>
<link rel="icon" type="image/ico" href="/dating/favicon.ico"> </link>
</head>
<body bgcolor="<?php echo $_SESSION['background']; ?>">

<?php include('menu.php'); ?>
<hr>

<?php 
	if ($_SESSION['username'] == "Admin") { 
		echo $_SESSION['query'], " ", $_SESSION['order'], "<br>"; 
	} // ADMIN TRICK 
?> 

<b><?php echo $lang['match'];?>...</b><br />
<?php echo $lang['found1'], ' ', ($res === false ? 0 : mysql_num_rows($res)), ' ', $lang['found2'], ( $res===false || mysql_num_rows($res) != 1 ? $lang['found4'] : $lang['found3'] ), ' ', $lang['found5']; ?> <br>
<?php 
if ($res === false || mysql_num_rows($res) == 0) { 
	echo $lang['nomatch']; } else {?>
<br><table >
<tr><td width=100><b><?php echo $lang['name']; ?></b></td>
<td width=60><b><?php echo $lang['age']; ?></b></td>
<td width=65><b><?php echo $lang['gender']; ?></b></td>
<td width=70><b><?php echo $lang['prefer']; ?></b></td>
<td width=70><b><?php echo $lang['province']; ?></b></td>
<td width=90><b><?php echo $lang['hair']; ?></b></td>
<td width=100><b><?php echo $lang['skin']; ?></b></td>
<td width=80><b><?php echo $lang['eye']; ?></b></td>
<td width=70><b><?php echo $lang['religion']; ?></b></td>
<td width=80><b><?php echo $lang['school']; ?></b></td>
<td width=70><b><?php echo $lang['kids']; ?></b></td>
<td width=90><b><?php echo $lang['wish']; ?></b></td>
<td width=50><b><?php echo $lang['smoke']; ?></b></td>
</tr>
<?php
for ($i=0; $row = mysql_fetch_assoc($res); $i++) 
	{ echo "<tr>
		<td> " . ($i + 1) . ". <a href='profile.php?" . ( isset($_GET['lang']) ? "lang=".$_GET['lang']."&" : "") ."u=".  $row['Username']."'>".$row['Username']." </td> 
		<td> " . calcAge2($row['Birthday']) . "</td>
		<td> " . $gender[$row['Gender']] . " </td>
		<td> " . $prefer[$row['Prefsex']] . " </td>
		<td> " . $province[$row['Provincie']] . " </td>
		<td> " . $hair[$row['Haarkleur']] . " </td>
		<td> " . $skin[$row['Huidskleur']] . " </td>
		<td> " . $eye[$row['Oogkleur']] . " </td>
		<td> " . $religion[$row['Religie']] . " </td>
		<td> " . $school[$row['Opleiding']] . " </td>
		<td style='text-align: center;'> " . $kids[$row['Kinderen']] . " </td>
		<td style='text-align: center;'> " . $wish[$row['Kinderwens']] . " </td>
		<td> " . $smoke[$row['Roken']] . " </td>
		</tr>"; }
?>
</table><br>

<table>
<form action="" method="post">
<input type="hidden" name="controle" value="TRUE">
<tr><td><?php echo $lang['sort']; ?> </td>
<td> <?php
$sorter = array("Birthday", "Geslacht", "Prefsex", "Provincie", "Haarkleur", "Oogkleur", "Huidskleur", "Religie", "Opleiding", "Kinderen", "Kinderwens", "Roken");
echo '<select name="sort">';
for($srcn=0; $srcn<12; $srcn++) {
	if($sorter[$srcn] == $_POST['sort']) {$selected='selected="selected"'; } else {$selected='';}
	echo '<option value="'.$sorter[$srcn].'" '.$selected. '>' . $sort[$srcn] . '</option>'; }
echo '</select>';

$way = array("ASC", "DESC");
echo '<select name="way">';
for($asc=0; $asc<2; $asc++) {
	if($way[$asc] == $_POST['way']) {$selected='selected="selected"'; } else {$selected='';}
	echo '<option value="'.$way[$asc].'" '.$selected. '>' . $wayer[$asc] . '</option>'; }
echo '</select>';?>
<input type="submit" value="Sort!"></form>
</table><br>
<?php } ?>

<form action="matching.php" method="post">
<input type="submit" value='<?php echo $lang['back']; ?>'></form> 
<hr>
<form action="profile.php" method="post">
<input type="submit" value='<?php echo $lang['profile'];?>'></form>
</body>
</html>