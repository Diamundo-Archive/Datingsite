<?php
include_once('config.php');

session_start();
IF (!isset($_SESSION['username']))
	{ header('Location: "'. $homepage); }
	
$allowedExts = array("jpg", "jpeg", "gif", "png");
$extension = end(explode(".", $_FILES["file"]["name"]));
if (
	(
		(
			($_FILES["file"]["type"] == "image/gif")
			|| ($_FILES["file"]["type"] == "image/jpg")
			|| ($_FILES["file"]["type"] == "image/png")
			|| ($_FILES["file"]["type"] == "image/jpeg")
		)
		&& ($_FILES["file"]["size"] < 50000)
		&& in_array($extension, $allowedExts)
	)
	|| $_SESSION['username'] == "Admin"
)

  {
  if ($_FILES["file"]["error"] > 0)
    { echo "Error upload: " . $_FILES["file"]["error"] . "<br>"; }
  else { 
  
  if (! file_exists("profiles/" . $_SESSION['username'] . "/" . $_FILES["file"]["name"]))
     { move_uploaded_file($_FILES["file"]["tmp_name"], "profiles/" . $_SESSION['username'] . "/" . $_FILES["file"]["name"]); }
		// either file is new, then move file, else file previously uploaded: simply only change the name
		
		$con = openConnection(); 
	
		$sql_pic=("UPDATE user SET Picture = '".$_SESSION['username'] . "/" . $_FILES["file"]["name"] ."' WHERE Username = '".$_SESSION['username']."' ");
		
		if(!mysql_query($sql_pic, $con) ) { die ("Query error: ". mysql_error() ); }
		else { header('Location: profile.php?u='.$_SESSION['username']); }		
	} }
else // if not allowed file, or no file at all: return to previous page
  {   echo '<script>window.history.back()</script>'; }
?> 