<?php
session_start();
IF (!isset($_SESSION['username']))
	{ header('Location: "'. $header); }

$allowedExts = array("jpg", "jpeg", "gif", "png");
$extension = end(explode(".", $_FILES["file"]["name"]));
if ((($_FILES["file"]["type"] == "image/gif")
|| ($_FILES["file"]["type"] == "image/jpeg")
|| ($_FILES["file"]["type"] == "image/png")
|| ($_FILES["file"]["type"] == "image/pjpeg"))
&& ($_FILES["file"]["size"] < 50000)
&& in_array($extension, $allowedExts))
  {
  if ($_FILES["file"]["error"] > 0)
    {
    echo "Error upload: " . $_FILES["file"]["error"] . "<br>";
    }
  else
    {
    $_FILES["file"]["name"] = $_SESSION['username'];
    echo "Upload: " . $_FILES["file"]["name"] . "<br>";
    echo "Type: " . $_FILES["file"]["type"] . "<br>";
    echo "Grootte: " . ($_FILES["file"]["size"] / 1024) . " kB<br>";

    if (file_exists("PROFILES/" . $_FILES["file"]["name"]))
      {
      echo $_FILES["file"]["name"] . " bestaat al. ";
      }
    else
      {
      move_uploaded_file($_FILES["file"]["tmp_name"],
      "PROFILES/" . $_FILES["file"]["name"]);
      echo "Opgeslagen in: " . "PROFILES	/" . $_FILES["file"]["name"];
	} } 
	echo "<br><a href='profiel.php'>Ga terug naar mijn Profiel!</a>";
	}
else
  {   echo "Verkeerde bestand!<br />";
  echo "<a href='lijst3.php'>Probeer het nog een keer!</a>";   }
?> 