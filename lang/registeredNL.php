<?php
$lang = array();
$lang['register'] = "Registratie";
$lang['database'] = "De volgende gegevens zijn toegevoegd aan de database:";
$lang['username'] = "Gebruikersnaam";
$lang['email'] = "E-mail adres";
$lang['birthday'] = "Geboortedatum";
$lang['login'] = "Log in!";
$lang['password'] = "Wachtwoord";
$lang['homepage'] = "Startpagina";

?>