<?php

$error_voornaam_empty;
$error_lastname_empty;
$error_gender_empty;
$error_prefsex_empty;
$error_province_empty;
$error_religion_empty;
$error_school_empty;
$error_skin_empty;
$error_hair_empty;
$error_eye_empty;
$error_smoke_empty;
$error_kids_empty;
$error_wish_empty;

$lang = array();
$lang['filling'];
$lang['firstname'];
$lang['lastname'];
$lang['gender'];
$lang['prefer'];
$lang['province'];
$lang['religion'];
$lang['school'];
$lang['skin'];
$lang['hair'];
$lang['eye'];
$lang['smoke'];
$lang['kids'];
$lang['wish'];
$lang['send'];
?>